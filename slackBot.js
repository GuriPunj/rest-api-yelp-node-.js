//"StAuth10065: I Gurpreet Singh Punj, 000358453 certify that this material is my original work. No other person's work has been used without due acknowledgement.
//I have not made my work available to anyone else."

var Bot = require('slackbots');
var express = require('express');
var https = require('https');
var querystring = require('query-string');
var bodyParser = require('body-parser');

var app = express();
var saveVal = "FALSE";
// parse application/json
app.use(bodyParser.json());

var aliasName = "";
var phoneNumberDisplay = "";
var businessId;
// create a bot
var settings = {
  token: 'xoxb-259045981299-A15ZYBMtXuAS80FoiusHVrjK',
  name: 'yelphelp'
};

var bot = new Bot(settings);
var yelpBotID = "";

bot.on('start', function()
{
  var usersJSON = bot.getUsers();
  var userMembers = usersJSON["_value"]["members"];

  for (var i=0;i<userMembers.length;i++)
  {
    if(userMembers[i].name =="yelphelp")
    {
      yelpBotID = userMembers[i]["profile"]["bot_id"];
    }
  }
});

bot.on('message', function(data)
{
  //the message is from a user
  if(data.type=="message" && data.text != "undefined")
  {
    if(data.bot_id != yelpBotID) {
      switch(data.text.split(' ')[0].toLowerCase())
      {
        case "nearby":
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();
        getNearbyAddressInfo(param,"");
        break;

        case "closeby":
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();
        long = param.split(' ')[0];
        lat = param.split(' ')[1];

        var isWest = long.replace(/[0-9]/g, ''); //for  longitude
        var isSouth = lat.replace(/[0-9]/g, '');//for latitude

        lat = parseFloat(lat.match(/[\d\.]+/))
        long = parseFloat(long.match(/[\d\.]+/))

        if(isWest=='.W')
        {
          long = long * -1;
        }
        else{
          long = long*1;
        }
        if(isSouth == '.S')
        {
          lat = lat * -1;
        }
        else{
          lat = lat*1;
        }
        getclosebyLongLatInfo(lat,long);
        break;


        case "top":
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();

        var topNumber = param.substr(0,param.indexOf(' '));
        topNumber = topNumber.trim();

        param = data.text.replace(data.text.split(' ')[0],'');
        param = data.text.replace(data.text.split(' ')[1],'');
        var parts = param.split(' ');
        parts.shift(); // parts is modified to remove first word

        var address;
        if (parts instanceof Array) {
          result = parts.join(' ');
        }
        else {
          result = parts;
        }
        address = result.trim();

        getTopXnumberAddInfo(topNumber, address);
        //Top 10 135 Fennel Avenue West, Hamilton, Ontario
        break;

        case "closest":
        //Closest 7 135 Fennel Avenue West, Hamilton, Ontario
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();

        var closestNumber = param.substr(0,param.indexOf(' '));
        closestNumber = closestNumber.trim();

        param = data.text.replace(data.text.split(' ')[0],'');
        param = data.text.replace(data.text.split(' ')[1],'');
        var parts = param.split(' ');
        parts.shift(); // parts is modified to remove first word

        var address;
        if (parts instanceof Array) {
          result = parts.join(' ');
        }
        else {
          result = parts;
        }
        address = result.trim();

        getClosestXnumberAddInfo(closestNumber, address);
        break;

        case "findme":
        //FindMe sushi 135 Fennel Avenue West, Hamilton, Ontario
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();

        var alias = param.substr(0,param.indexOf(' '));
        alias = alias.trim();

        param = data.text.replace(data.text.split(' ')[0],'');
        param = data.text.replace(data.text.split(' ')[1],'');
        var parts = param.split(' ');
        parts.shift(); // parts is modified to remove first word

        var address;
        if (parts instanceof Array) {
          result = parts.join(' ');
        }
        else {
          result = parts;
        }
        address = result.trim();
        aliasName = alias;

        getFindMeCategoryAddInfo(alias, address);
        break;

        case "searchbyphone":
        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();
        phoneNumber = param;
        phoneNumberDisplay = "+".concat(String(phoneNumber))
        getSearchByPhoneInfo(phoneNumber);
        break;

        case "reviews":
        //Reviews Spring Sushi 135 Fennel Avenue West, Hamilton, Ontario

        var param = data.text.replace(data.text.split(' ')[0],'');
        param = param.trim();
        var RestaurantName ="";
        RestaurantName = param.split(' ')[0];

        var isNumber =  /^\d+$/.test(param.split(' ')[1]);

        if(isNumber == false)
        {
          RestaurantName+= " ".concat(param.split(' ')[1]);
        }

        param = param.replace(param.split(' ')[0],"");
        if(isNumber == false)
        {
          param = param.replace(param.split(' ')[1],"");
        }

        param = param.trim();

        getNearbyAddressInfo(param, RestaurantName);
        //getReviewsInfo(RestaurantName,param);
        break;

        default:
        bot.postMessageToChannel('general', 'Please check the entered command!!');
        break;
      }
    }
  }
});

function getNearbyAddressInfo(param,resName)     {
var myreq;
  if(resName != "")
  {
    saveVal = "TRUE";
   myreq = {
      location : String(param),
      term : resName,
    };
  }
  else {
    myreq = {
      location : String(param),
      limit : 6,
      radius : 10000,
    };
  }

  getBasicYELPInfo(myreq,"Nearby");
}

function getclosebyLongLatInfo(lat,long)    {
  var myreq = {
    latitude : lat,
    longitude : long,
    limit : 6,
    radius : 10000,
  };
  getBasicYELPInfo(myreq,"Closeby");
}

function getTopXnumberAddInfo(topNumber, address)     {
  var myreq = {
    location : String(address),
    limit : topNumber,
    radius : 10000,
    sort_by : "rating"
  };
  getBasicYELPInfo(myreq,"TopXNumber");
}

function getClosestXnumberAddInfo(closestNumber, address) {
  var myreq = {
    location : String(address),
    limit : closestNumber,
    radius : 10000,
    sort_by : "distance"
  };

  getBasicYELPInfo(myreq,"ClosestNumber");
}

function getFindMeCategoryAddInfo(alias, address) {
  var myreq = {
    location : String(address),
    radius : 20000,
    alias : String(alias)
  };
  getBasicYELPInfo(myreq,"FindMe");
}

function getSearchByPhoneInfo(phoneNumber)     {
  var myreq = {
    phone : "+".concat(String(phoneNumber)),
  };
  getBasicYELPInfo(myreq,"searchbyphone");
}


function getReviewsInfo(id)           {
  var myreq = "reviews";

  getBasicYELPInfo(myreq,"Reviewsinfo");
}

function getBasicYELPInfo(myreq,type)
{
  /******************************************************************************
  *
  * Use the access_token to make a request to the YELP API.
  *
  ******************************************************************************/
  function testfunc(access_token)
  {
    // stringify our request parameters
    if(myreq !="reviews")
    {
      var myreqstr = querystring.stringify(myreq);
    }

    // setup options for the request (note that we use https)

    var options;
    if(type=="Reviewsinfo")
    {
      //https://api.yelp.com/v3/businesses/{id}/reviews
      options = {
        host: 'api.yelp.com',
        port: '443',
        path: '/v3/businesses/' + businessId + '/reviews', // include the URL parameters
        method: 'get',
        headers : {
          'Authorization' : 'Bearer ' + access_token,
        }
      };
    }
    if(type=="searchbyphone")
    {
      options = {
        host: 'api.yelp.com',
        port: '443',
        path: '/v3/businesses/search/phone?' + myreqstr, // include the URL parameters
        method: 'get',
        headers : {
          'Authorization' : 'Bearer ' + access_token,
        }
      };
    }
    else {
      options = {
        host: 'api.yelp.com',
        port: '443',
        path: '/v3/businesses/search?' + myreqstr, // include the URL parameters
        method: 'get',
        headers : {
          'Authorization' : 'Bearer ' + access_token,
        }
      };
    }

    // call back function will dump out results
    mycallback = function(response) {
      var str = '';

      //another chunk of data has been recieved, so append it to `str`
      response.on('data', function (chunk) {
        str += chunk;
      });

      //the whole response has been recieved, so we just print it out here
      response.on('end', function () {
        var result = JSON.parse(str);
        //the try catch to see if there are any results found or not
        try{
          if(type=="Reviewsinfo")
          {
            console.log(result);
            for (var i=0;i<result.reviews.total;i++)
            {
              //return the review excerpt text, reviewer username, rating, and link to the full review
              console.log("Rating:" + result.reviews[i].rating);
              console.log("User Name:" + result.reviews[i].user["name"]);
              console.log("Text:" + result.reviews[i].text);
              console.log("Link:" + result.reviews[i].url);

            }
            if(result.businesses.length<=0)
            {
              throw "exception";
            }
          }

          if(type=="FindMe")
          {
            for (var i=0;i<result.businesses.length;i++)
            {
              bot.postMessageToChannel('general', "Name:" + result.businesses[i].name);
              bot.postMessageToChannel('general', "Address:" + result.businesses[i].location["display_address"]);
              bot.postMessageToChannel('general', "Rating:" + result.businesses[i].rating);
              //console.log("Name:" + result.businesses[i].name);
              //console.log("Address:" + result.businesses[i].location["display_address"]);
              //console.log("Rating:" + result.businesses[i].rating);
            }
            if(result.businesses.length<=0)
            {
              throw "exception";
            }
          }

          else {
            if(saveVal=="TRUE")
            {
              for (var i=0;i<1;i++)
              {
                businessId = result.businesses[i].id;
              }
              getReviewsInfo(businessId);
              return;
            }
            else {
              for (var i=0;i<result.businesses.length;i++)
              {
                bot.postMessageToChannel('general', "Name:" + result.businesses[i].name);
                bot.postMessageToChannel('general', "Address:" + result.businesses[i].location["display_address"]);
              }
              if(result.businesses.length<=0)
              {
                throw "exception";
              }
            }

          }
        }

        catch(err)
        {
          if(type=="searchbyphone")
          {
            bot.postMessageToChannel('general', "No nearby restaurants can be found with phone number" +  phoneNumberDisplay);
            //console.log("No nearby restaurants can be found with phone number" +  phoneNumberDisplay);
          }
          if(type=="FindMe")
          {
            bot.postMessageToChannel('general', "No nearby restaurants can be found with category" + aliasname);
            //console.log("No nearby restaurants can be found with category" + aliasname);
          }
          if(type=="TopXNumber" || type=="ClosestNumber")
          {
            bot.postMessageToChannel('general', "Couldn't find more restaurants");
            //console.log("Couldn't find more restaurants");
          }
          else {
              bot.postMessageToChannel('general', "No nearby restaurants can be found");
          }
        }
      });
    }

    // make the request
    var newreq = https.request(options, mycallback).end();
  }


  /******************************************************************************
  *
  * Use client_id (App ID) and client_secret (App Secret) to obtain an access
  * token for the YELP API.
  *
  ******************************************************************************/
  callback = function(response) {
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      var tokens = JSON.parse(str);

      // once we have the access token, make a request to the API
      testfunc(tokens.access_token);
    });
  }

  // setup request with app id, app secret
  var authreq = {
    'grant_type' : "client_credentials",
    'client_id' : "a3WvT1FpVC1ej5fDjciPdg", // put the App ID here,
    'client_secret' : "gvdZPkcqJHKPpNNmGC3w8v4XlUCh0OVBZyFXdUKJclPVsWyfXyFMpYXLYRwzBhjj" // put the App secret here
  };

  var authreqstr = querystring.stringify(authreq);

  var authoptions = {
    host: 'api.yelp.com',
    port: '443',
    path: '/oauth2/token',
    method: 'POST',
    headers: {
      'Content-Type':'application/x-www-form-urlencoded',
      'Content-Length' : Buffer.byteLength(authreqstr)
    }
  };

  // send the post data
  var req = https.request(authoptions, callback);
  req.write(authreqstr);
  req.end();
}
